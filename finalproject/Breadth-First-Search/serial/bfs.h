#if !defined(BFS_H)
#define      BFS_H

#include <stddef.h>
#include <stdint.h>

/*
 * This defines `cse6230graph` to be an *opaque pointer* to an unspecified
 * struct.  In your implementation, you should define this struct to hold
 * whatever data you want to define a graph type.  Because the routines in
 * this interface do not *dereference* the pointer (directly access the data
 * members), we will be able to use the same interface for everyone's
 * implementation.
 */
typedef struct _cse6230graph * cse6230graph;

int graph_create(size_t num_edges, const int64_t (* edges)[2], cse6230graph *graph_p);
int breadth_first_search(cse6230graph graph, int num_keys, const int64_t *key, int64_t **parents);
int graph_destroy(cse6230graph *graph_p);
#endif
