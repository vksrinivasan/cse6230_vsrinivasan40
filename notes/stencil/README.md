# Stencil Computation

## Cache blocking

Please read about the "layer condition" for cache blocking in stencils in the
slides linked below.

In class, I had difficulty demonstrating the benefits of the "layer condition"
for increasing the lattice updaters per second of the Jacobi smoother.  I
believe this is because the dropoff in effecive bandwidth from the L2 to the L3
cache on the Intel Kaby Lake processor in my laptop is almost nonexistent (see
my STREAM bandwidths in Exercise 01, where `COPY` is the only command that
distinguishes between the two). 

This means that the layer condition should only have a noticeable impact on the
performance of the Jacobi smoother from the perspective of the L3 cache.
Recall that the layer condition for reducing the loads per lattice update in
the 2D stencil is `3 * jblock * 8B < cache_size / 2`.  My L3 cache has ~3MB,
which means that the condition is `jblock < 62500`.

The current version of the jacobi example is called with the form `./jacobi M N
JBLOCK NSTEPS`.  Here are the results without blocking for values of `N` around
62500:

| N      | LUPs  |
| ---    | ---   |
| 16002  | 7.6e8 |
| 32002  | 7.5e8 |
| 64002  | 7.1e8 |
| 128002 | 5.7e8 |
| 256002 | 5.0e8 |

So we do see the performance start to dip right around where the layer condition predicts.
Now let's see if cache blocking helps: here is a table for `N=256002` with different values
of `jblock`:

| jblock | LUPs  |
| ---    | ---   |
| 128000 | 5.6e8 |
| 64000  | 7.0e8 |
| 32000  | 7.3e8 |
| 16000  | 7.3e8 |
| 8000   | 7.4e8 |

So we do recover most of the "small" array performance, even for the larger array dimensions.

## Additional Reading

- [Wellein, Hager & Wittman on 2D Jacobi](https://moodle.rrze.uni-erlangen.de/pluginfile.php/13130/mod_resource/content/2/10_06-09-2017_PTfS.pdf)
